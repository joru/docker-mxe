FROM debian:11

ARG MXE_TARGETS='x86_64-w64-mingw32.static'

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
      autoconf \
      automake \
      autopoint \
      bash \
      bison \
      bzip2 \
      flex \
      g++ \
      g++-multilib \
      gettext \
      git \
      gperf \
      intltool \
      libc6-dev-i386 \
      libgdk-pixbuf2.0-dev \
      libltdl-dev \
      libssl-dev \
      libtool-bin \
      libxml-parser-perl \
      lzip \
      make \
      openssl \
      p7zip-full \
      patch \
      perl \
      pkg-config \
      python \
      ruby \
      sed \
      unzip \
      wget \
      xz-utils \
      python3-mako

RUN groupadd mxe
RUN useradd -g mxe mxe

RUN git clone https://github.com/mxe/mxe.git /mxe
RUN chown mxe:mxe -R /mxe

USER mxe
WORKDIR /mxe

RUN make MXE_TARGETS="$MXE_TARGETS" qt5 qtifw -j --keep-going

FROM debian:11
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
      autoconf \
      automake \
      autopoint \
      bash \
      bison \
      bzip2 \
      flex \
      g++ \
      g++-multilib \
      gettext \
      git \
      gperf \
      intltool \
      libc6-dev-i386 \
      libgdk-pixbuf2.0-dev \
      libltdl-dev \
      libssl-dev \
      libtool-bin \
      libxml-parser-perl \
      lzip \
      make \
      openssl \
      p7zip-full \
      patch \
      perl \
      pkg-config \
      python \
      ruby \
      sed \
      unzip \
      wget \
      xz-utils

RUN groupadd mxe
RUN useradd -g mxe mxe

USER mxe
WORKDIR /mxe

COPY --from=0 /mxe/usr /mxe/usr

